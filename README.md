# vg5000

This is a clone of the VG5000 font repository: https://gitlab.com/velvetyne/vg5000
The goal is to learn font design through making it into a variable font.